INSTALLATION INSTRUCTIONS
=================================

* How to add to your path variable on windows: http://www.computerhope.com/issues/ch000549.htm
* Link to scons: http://scons.org/

1. Ensure that your python 2.7 install is 64 bits and is in your system path.
  1. Open a command line and type `python`
  2. Type these two commands into the python shell: `import platform` and `platform.architechture()`
  3. Observe that the output says `64bit`
2. Ensure you have a C compiler.
  * If on windows you must have a version of visual studio installed (2015 community works just fine.) 
  * During the install you should have the C++ compiler selected, if you do not, rerun the installation and select "Common tools for C++" to update it.
3. Ensure that scons is installed and the Python27\Scripts directory is in your path
  1. Enter the opa\src\modules directory and type `scons`
  2. This should compile the opa dlls, if it does not, check your scons installation or path variable.
  

