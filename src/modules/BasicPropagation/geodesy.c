#include "geodesy.h"

// Spherical earth height decrease at distance
// This is a simple formulation based upon a perfectly spherical earth
// and shows from a stationary point to a point 'distanceMeters' away
// how far that point has 'dropped'
OPA_EXPORT double EarthCurveHeightDecrease(double distanceMeters) {
  return (MEAN_EARTH_RADIUS_METERS - sqrt(pow(MEAN_EARTH_RADIUS_METERS,2.0) - pow((distanceMeters),2.0)));
}

double Haversine(double theta) {
  return (1.0 - cos(theta)) / 2;
}

double HaversinInverse(double hav) {
  return 2 * asin(sqrt(hav));
}

OPA_EXPORT struct GeoResult HaversineGeo(double latA, double lonA, double bearing, double dist) {
  struct GeoResult result;

  double angularDistance = dist / MEAN_EARTH_RADIUS_METERS;
  double latB = asin(sin(latA) * cos(angularDistance) + 
                    cos(latA) * sin(angularDistance) * cos(bearing));
  double lonB = lonA + atan2( sin(bearing) * sin(angularDistance) * cos(latA), 
                              cos(angularDistance) - sin(latA) * sin(latB));
  result.lat = latB;
  result.lon = lonB;
  result.bearing = fmod(bearing + M_PI ,2.0*M_PI); 
  return result;
}

// http://www.movable-type.co.uk/scripts/latlong.html
OPA_EXPORT double HaversineGeoInverse(double latA, double latB, double lonA, double lonB) {
  double havLat = Haversine(latB - latA); 
  double havLon = Haversine(lonB - lonA);
  return 2 * MEAN_EARTH_RADIUS_METERS * asin(sqrt(havLat + cos(latA)*cos(latB)*havLon));
}


// ------------------------------------------------------
// Below this are functions that haven't been finished.
// ------------------------------------------------------
//TODO FINISH -- A much more accurate model for distance on earth.
//http://link.springer.com/article/10.1007%2Fs00190-012-0578-z
//http://arxiv.org/abs/1102.1215
OPA_EXPORT struct InverseGeoResult KarneyGeoInverse(double latA, double latB, double lonA, double lonB) {
  //TODO FINISH
  struct InverseGeoResult kgr;
  return kgr;
}

/*#include<stdio.h>
int main() {
  struct GeoResult test;
  test = HaversineGeo(5.555 * M_PI / 180.0,5.555 * M_PI / 180.0,M_PI/4.0,5555000);
  printf("%f %f %f\n",test.lat * 180.0 / M_PI,test.lon * 180 / M_PI,test.bearing * 180 / M_PI);
}
*/

