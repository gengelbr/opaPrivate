#include "freespace.h"

OPA_EXPORT double FreeSpaceLoss(double frequencyHertz, double distanceMeters) {
	double num = 4.0 * M_PI * distanceMeters * frequencyHertz;
  return 20 * log10(num / SPEED_OF_LIGHT_VAC_METERS);
}
