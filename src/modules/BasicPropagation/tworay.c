#include "tworay.h"

// https://en.wikipedia.org/wiki/Two-ray_ground-reflection_model
// http://www.ti.com/lit/an/swra479/swra479.pdf
// Polarization 0 = vertical , Polarization 1 = horizontal 
// TODO THIS NEEDS TO BE VALIDATED.
OPA_EXPORT struct TwoRayResult TwoRay(double heightTXMeters, double heightRXMeters, double groundDistMeters, 
              int polarization,double groundCon, double frequency, double cosSigAmplitude) {

  double wavelengthMeters = SPEED_OF_LIGHT_VAC_METERS / frequency;
  
  // Find line of sight component distance
  double losDistMeters = sqrt(pow(heightTXMeters - heightRXMeters,2.0) +
                              pow(groundDistMeters,2.0));
  
  // Find reflected component total distance
  double reflectDistMeters = sqrt(pow(heightTXMeters + heightRXMeters,2.0) +
                                  pow(groundDistMeters,2.0));
  
  // Find reflection incidence angle i.e. the arctangent
  // of right triangle formed with ground and both heights
  double theta = atan( (heightTXMeters + heightRXMeters) / groundDistMeters);
  
  // Find ground reflection coefficient
  double groundReflectionPolarizationFactor;
  if(polarization == 0) {
    // vertical polarization 
    groundReflectionPolarizationFactor = sqrt(groundCon - cos(theta) * cos(theta)) / groundCon;
  }
  else if (polarization == 1) {
   // horizontal polarization
    groundReflectionPolarizationFactor = sqrt(groundCon - cos(theta) * cos(theta));
  }
  else {
    // TODO Still haven't made a good error reporting convention
    exit(1); 
  }
  double groundReflectionCoeff = (sin(theta) - groundReflectionPolarizationFactor) / 
                                  (sin(theta) + groundReflectionPolarizationFactor);
  
  // above val might be NaN, all NaN compares == false, therefor...
  if(groundReflectionCoeff != groundReflectionCoeff) {
    groundReflectionCoeff = 0.0; //perfectly absorbing ground ;)
  }
  double groundReflectionSign = (groundReflectionCoeff < 0) ? 
                                -1 
                                : 
                                (groundReflectionCoeff > 0);
  
  // Find phase difference
  double phaseDelay=  (2 * M_PI * (reflectDistMeters - losDistMeters) / wavelengthMeters);
  phaseDelay = fmod(phaseDelay,2*M_PI);
  
  // Find amplitude at phase of the sinusoid
  double ampAtPhaseDifference = cos(phaseDelay) * groundReflectionSign * cosSigAmplitude;

  // Find line of sight amplitude
  #ifdef _WIN32
	_Dcomplex tmp = _Cmulcr(_Cbuild(0.0,1), 2.0);
	tmp = _Cmulcr(tmp, M_PI);
	tmp = _Cmulcr(tmp, losDistMeters);
	tmp = _Cmulcr(tmp, 1.0 / wavelengthMeters);
	tmp = cexp(tmp);
	double losexp = creal(tmp);
  #else
	double losexp = creal(cexp((I * 2 * M_PI*losDistMeters) / wavelengthMeters));
  #endif
  double losRayAmpNum = wavelengthMeters * cosSigAmplitude * losexp;
  double losRayAmpDenom = 4 * M_PI * losDistMeters;
  double losRayAmp = losRayAmpNum / losRayAmpDenom;

  // Find ground bounce amplitude
  #ifdef _WIN32
	tmp = _Cmulcr(_Cbuild(0.0, -1), 2.0);
    tmp = _Cmulcr(tmp, M_PI);
	tmp = _Cmulcr(tmp, reflectDistMeters);
	tmp = _Cmulcr(tmp, 1.0 / wavelengthMeters);
	tmp = cexp(tmp);
	double gndexp = creal(tmp);
  #else
	double gndexp = creal(cexp((-I * 2 * M_PI*reflectDistMeters) / wavelengthMeters));
  #endif
  double gndRayAmpNum = wavelengthMeters * ampAtPhaseDifference 
						* groundReflectionCoeff * gndexp;
  double gndRayAmpDenom = 4 * M_PI * reflectDistMeters;
  double gndRayAmp = gndRayAmpNum / gndRayAmpDenom;

  // Combine for power.
  double powerdBW = 10*log10(pow(losRayAmp + gndRayAmp,2.0));

  struct TwoRayResult result;
  result.phaseDelay = phaseDelay;
  result.losRayAmp = losRayAmp;
  result.gndRayAmp = gndRayAmp;
  result.powerdBW = powerdBW;
  return result;
};

/*
int main() {
  double i;
  for(i = 1; i < 500000; i = i+100.0) {
    double los = TwoRay(100,50,i,0,10,3500e6,1000);
  }
  printf("Drop @%f : %f \n",6371000000.0,EarthCurvAntHeightDecrease(6371000000.0));
  return 0;
}
*/
