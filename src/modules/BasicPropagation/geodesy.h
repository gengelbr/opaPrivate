#include <math.h>

#include "common.h"

//Structs
struct GeoResult {
  double lat;
  double lon;
  double bearing;
};

struct InverseGeoResult {
  double bearingA;
  double bearingB;
  double dist;
};

// Internal Functions
double Haversine(double theta);
double HaversinInverse(double hav);

//External Functions
OPA_EXPORT double EarthCurveHeightDecrease(double distanceMeters);
OPA_EXPORT struct GeoResult HaversineGeo(double latA, double lonA, double bearing, double dist);
OPA_EXPORT double HaversineGeoInverse(double latA, double latB, double lonA, double lonB);

