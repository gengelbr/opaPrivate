#include <math.h>
#include <stdlib.h>
#include <complex.h>
#include <stdio.h>

#include "common.h"

struct TwoRayResult {
  double phaseDelay;
  double losRayAmp;
  double gndRayAmp;
  double powerdBW;
};

OPA_EXPORT struct TwoRayResult TwoRay(double heightTXMeters, double heightRXMeters, 
                           double groundDistMeters, int polarization, 
                           double groundCon, double frequency, double cosSigAmplitude);
