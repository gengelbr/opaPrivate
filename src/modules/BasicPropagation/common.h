#ifndef COMMON_H
#define COMMON_H


#ifdef _WIN32
#  define OPA_EXPORT __declspec( dllexport )
#else
#  define OPA_EXPORT
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif
#define MEAN_EARTH_RADIUS_METERS 6371008.8 
#define SPEED_OF_LIGHT_VAC_METERS 299792458.0



#endif
