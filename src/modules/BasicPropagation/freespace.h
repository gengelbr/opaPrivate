#include<math.h>

#include "common.h"

// External Functions
OPA_EXPORT double FreeSpaceLoss(double frequencyHertz, double distanceMeters);
