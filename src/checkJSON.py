#!/usr/bin/python

import os
import json
# traverse root directory, and list directories as dirs and files as files
for root, dirs, files in os.walk("."):
  path = root.split('/')
  print (len(path) - 1) *'---' , os.path.basename(root)
  for file in files:
    if file[-4:] == 'json':
      print "Checking ",len(path)*'---', file
      f = open(os.path.join(root,file))
      json.load(f)
