import math
import copy
from decimal import *

# Easily accessible infs
inf = float("inf")
nan = float("nan")

Decimal10 = Decimal(10)

COMPARISON_EPSILON = 1e-10
MIN_DB_LIN_VAL = 1e-27

PRINT_QUANTIZE = Decimal('0.000001')
PRINT_ROUNDING = ROUND_UP

Units = []

class Unit(object):
  global Units
  SIPrefixToN = {
    "yotta":24,
    "zetta":21,
    "exa":18,
    "peta":15,
    "tera":12,
    "giga":9,
    "mega":6,
    "kilo":3,
    "" : 0,
    "centi":-2,
    "milli":-3,
    "micro":-6,
    "nano":-9,
    "pico":-12,
    "femto":-15,
    "atto":-18,
    "zepto":-21,
    "yocto":-24
  }
  SIPrefixToSymb = {
    "yotta":"Y",
    "zetta":"Z",
    "exa":"E",
    "peta":"P",
    "tera":"T",
    "giga":"G",
    "mega":"M",
    "kilo":"k",
    "" : "",
    "centi":"c",
    "milli":"m",
    "micro":"u",
    "nano":"n",
    "pico":"p",
    "femto":"f",
    "atto":"a",
    "zepto":"z",
    "yocto":"y"
  }
  NToSIPrefix = {
    24:"yotta",
    21:"zetta",
    18:"exa",
    15:"peta",
    12:"tera",
    9:"giga",
    6:"mega",
    3:"kilo",
    0:"",
    -2:"centi",
    -3:"milli",
    -6:"micro",
    -9:"nano",
    -12:"pico",
    -15:"femto",
    -18:"atto",
    -21:"zepto",
    -24:"yocto"
  }
  SymbToSIPrefix = {
    "Y":"yotta",
    "Z":"zetta",
    "E":"exa",
    "P":"peta",
    "T":"tera",
    "G":"giga",
    "M":"mega",
    "k":"kilo",
    "" : "",
    "c":"centi",
    "m":"milli",
    "u":"micro",
    "n":"nano",
    "p":"pico",
    "f":"femto",
    "a":"atto",
    "z":"zepto",
    "y":"yocto"
  }

  def __init__(self, id=None,isdb=False, dbRelLinUnit=None,
      	       dbRelLinPrefix=None, validRange=(-inf,inf), initialPrefixN=0):
    self.isdb = isdb
    self.id = id
    self.validRange = validRange
    self._value = None
    self.db = {}
    
    if isdb:
      self.dbRelLinUnit = dbRelLinUnit
      self.dbRelLinPrefix = dbRelLinPrefix
      self.dbNegative = False
    else:
      self.currentPrefixN = initialPrefixN
      for x in Unit.NToSIPrefix.keys():
        self.db[x] = \
          Unit(id='db-'+Unit.NToSIPrefix[x]+id,isdb=True,
               dbRelLinUnit=self,dbRelLinPrefix=x)
        
    Units.append(self)

  @property
  def value(self):
    return self._value

  @value.setter
  def value(self,val):
    self._value = Decimal(val) if (val is not None) else None

  @staticmethod
  def LintodB(val,linPrefix,relLinPrefix):
    if abs(val) < MIN_DB_LIN_VAL:
      return -inf
    return (abs(val) * Decimal10**linPrefix /
                      Decimal10**relLinPrefix).log10() * Decimal10

  def toLinear(self,andPrefix = 0):
    if not self.isdb:
      return copy.copy(self).changePrefix(andPrefix)
    else:
      cop = copy.copy(self.dbRelLinUnit)
      neg = Decimal(-1.0) if self.dbNegative else Decimal(1.0)
      cop.value = neg * Decimal10**(self.value / Decimal10) * Decimal10**self.dbRelLinPrefix
      return cop.changePrefix(andPrefix)

  def asDB(self,relativedB):
    if isinstance(relativedB,int):
      if self.isdb:
        relativedB = self.dbRelLinUnit.db[relativedB]
      else:
        relativedB = self.db[relativedB]
    if Unit.__compat(self,relativedB):
      cop = copy.copy(relativedB)
      if self.isdb:
        lin = self.toLinear()
        cop.dbNegative = True if lin.value < 0 else False
        cop.value = Unit.LintodB(abs(lin.value),lin.currentPrefixN,relativedB.dbRelLinPrefix)
      else:
        cop.dbNegative = True if self.value < 0 else False
        cop.value = Unit.LintodB(abs(self.value),self.currentPrefixN,relativedB.dbRelLinPrefix)
      return cop
    else:
      raise Exception("Cannot convert incompatible dB values")                                   
                      
  def __eq__(self,other):
    #TODO - This is written very inefficiently.
    baseSelf = self.toLinear(andPrefix=0)
    copyOther = other.toLinear(andPrefix=0)
    if abs(baseSelf.value - copyOther.value) < COMPARISON_EPSILON:
      return True
    else:
      return False

  def __ne__(self,other):
    return not self.__eq__(other)

  def __gt__(self,other):
    #TODO - This is written very inefficiently.
    baseSelf = self.toLinear(andPrefix=0)
    copyOther = other.toLinear(andPrefix=0)
    if (baseSelf.value - copyOther.value) > COMPARISON_EPSILON:
      return True
    else:
      return False

  def __lt__(self,other):
    return not self.__gt__(other)

  def __sub__(a,b):
    return a.__add__(-b)
  def __rsub__(a,b):
    return a.__sub__(b)

  @staticmethod
  def __compat(a,b):
    if b.isdb:
      if a.isdb:
          if b.dbRelLinUnit.id != a.dbRelLinUnit.id:
            return False
      else:
        if b.dbRelLinUnit.id != a.id:
          return False
      return True
    else:
      if a.isdb:
        if a.dbRelLinUnit.id != b.id:
          return False
      else:
        if a.id != b.id:
          return False
      return True

  @staticmethod
  def __reflect(a,b):
    '''Reflect the unit a as a unit of type b if it is compatable'''
    if Unit.__compat(a,b):
      if b.isdb:
        return a.asDB(b)
      else:
        return a.toLinear(b.currentPrefixN)
    else:
      raise Exception("Can't reflect incompatible Units.")

  def __resolveB(a,b):
    '''Attempt to create an instance of the left hand paramter from a scalar'''
    btmp = b
    if not isinstance(b,Unit):
      b = a(abs(b))
      return -b if btmp < 0 else b
    else:
      return b

  def __add__(a,b):
    '''The final unit and prefix always is taken by the first multiplicand'''
    b = a.__resolveB(b)
    newB = Unit.__reflect(b,a)
    linvalue = newB.toLinear().value + a.toLinear().value
    if a.isdb:
      return a.dbRelLinUnit(linvalue).asDB(a)
    else:
      return a(linvalue).changePrefix(a.currentPrefixN)
  def __radd__(a,b):
    return a.__add__(b)

  def __mul__(a,b):
    '''The final unit and prefix always is taken by the first multiplicand'''
    b = a.__resolveB(b)
    newB = Unit.__reflect(b,a)
    linvalue = newB.toLinear().value * a.toLinear().value
    if a.isdb:
      return a.dbRelLinUnit(linvalue).asDB(a)
    else:
      return a(linvalue).changePrefix(a.currentPrefixN)
  def __rmul__(a,b):
    return a.__mul__(b)

  def __neg__(a):
    tmp = copy.copy(a)
    if a.isdb:
      tmp.dbNegative = not tmp.dbNegative
    else:
      tmp.value = -tmp.value
    return tmp

  def __repr__(self):
    if self.isdb:
      neg = "deficit" if self.dbNegative else ""
      return str(self.value.quantize(PRINT_QUANTIZE,PRINT_ROUNDING)) + \
             " "+self.id + " " + neg
    else:
      return str(self.value.quantize(PRINT_QUANTIZE,PRINT_ROUNDING)) + \
             " "+Unit.NToSIPrefix[self.currentPrefixN]+self.id

  def __str__(self):
    if self.isdb:
      neg = "deficit" if self.dbNegative else ""
      return str(self.value.quantize(PRINT_QUANTIZE,PRINT_ROUNDING)) + \
             " "+self.id + " " + neg
    else:
      return str(self.value.quantize(PRINT_QUANTIZE,PRINT_ROUNDING)) + \
             " "+Unit.NToSIPrefix[self.currentPrefixN]+self.id

  def __call__(self,*args):
    if len(args) > 1:
      "Units may only have one argument!"
    arg = args[0]
    if math.isnan(arg):
      print "Warning: Value is nan (not a number)"
    if math.isinf(arg):
      print "Warning: Value is inf (infinite)"
    #if arg < self.validRange[0]:
    #  print "Warning: Value is below valid range: ",self.validRange
    #if arg > self.validRange[1]:
    #  print "Warning: Value is above valid range: ",self.validRange
    cop = copy.copy(self)
    cop.value = arg
    return cop

  def changePrefix(self,newPrefix):
    if newPrefix not in self.NToSIPrefix:
      raise Exception("We don't know the SI name for " + str(newPrefix))
    if not self.isdb:
      cop = copy.copy(self)
      cop.value =  self.value * Decimal10 ** self.currentPrefixN * Decimal10**-newPrefix
      cop.currentPrefixN = newPrefix
      return cop
    else:
      raise Exception("Cannot changePrefix on a dB")

Unitless = Unit(id="Unknown",validRange=(-inf,inf))
Radians = Unit(id="Radians",validRange=(-2.0 * math.pi,2.0 * math.pi))
Meters = Unit(id="Meters",validRange=(0,inf))
Hertz = Unit(id="Hertz",validRange=(0,inf))
Volts = Unit(id="Volts",validRange=(-inf,inf))
Seconds = Unit(id="Seconds",validRange=(-inf,inf))
Watts = Unit(id="Watts",validRange=(0,inf))
GeoRadians = Unit(id="GeoRads",validRange=(-inf,inf))


#Canonical representations
DB = Unitless.db[0]
DBm = Watts.db[-3]
DBW = Watts.db[0]

b = DBm(20)
a = DBW(5)

#GeoRadians = Unit(id="Geographic-radians",
#Prob = Unit(id="percent chance",shortId="%",validRange=(0,1))
#dbi = Unit(id="dbi",isdb=True,dbRelLinUnit= ,dbRelLinVal=1)
#volts
#meters
#rads
#position
#ohms
#siemens
#...henrys webbers tesla
# dbuv / m
# amps / m
