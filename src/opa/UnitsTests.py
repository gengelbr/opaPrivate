import unittest
import random
from Units import *

getcontext().prec = 60

def fuzzNum():
  return random.uniform(-100,100)

class TestUnits(unittest.TestCase):
  def test_construct_all(self):
    for UnitType in Units:
      #print 'Test constructing unit',str(UnitType)
      x = UnitType(fuzzNum())
      self.assertIsInstance(x,type(UnitType))

  def test_toLinear(self):
    for UnitType in Units:
      #print 'Test converting to linear unit',str(UnitType)
      x = UnitType(fuzzNum()).toLinear()
      self.assertFalse(x.isdb)

  def test_changePrefix(self):
    for UnitType in Units:
      orig = UnitType(fuzzNum())
      for x in Unit.SIPrefixToN.values():
        #print 'Test converting to SI Prefix unit',str(UnitType),'to',x
        z = orig.toLinear().changePrefix(x)
        self.assertTrue(z == orig)

  def test_addMath(self):
    for UnitType in Units:
      orig = UnitType(1)
      #print 'Test additive identity on unit',str(UnitType)
      if not UnitType.isdb:
        self.assertTrue(orig+orig == UnitType(2))
        self.assertTrue(orig-orig == UnitType(0))
      else:
        self.assertTrue(orig+orig == orig.toLinear() + orig.toLinear())

  def test_addCommAss(self):
    for UnitType in Units:
      orig = UnitType(fuzzNum())
      orig2 = UnitType(fuzzNum())
      orig3 = UnitType(fuzzNum())
      #print ((orig+orig2) + orig3).toLinear(),((orig3+orig2)+orig).toLinear()
      self.assertTrue(((orig+orig2) + orig3).toLinear() == ((orig3+orig2)+orig).toLinear())

  def test_mulCommAss(self):
    for UnitType in Units:
      orig = UnitType(fuzzNum())
      orig2 = UnitType(fuzzNum())
      orig3 = UnitType(fuzzNum())
      self.assertTrue(((orig*orig2)*orig3).toLinear() == (orig2*(orig*orig3)).toLinear())

  def test_mulLinAddDB(self):
    for UnitType in Units:
      orig = UnitType(fuzzNum())
      orig2 = UnitType(fuzzNum())
      orig3 = UnitType(fuzzNum())
      linorig = orig.toLinear()
      lin2orig = orig2.toLinear()
      lin3orig = orig3.toLinear()
      self.assertTrue(linorig+lin2orig+lin3orig == (orig2+orig+orig3).toLinear())
      
      
if __name__ == '__main__':
  unittest.main()
