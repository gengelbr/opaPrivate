import Units
import ctypes
import inspect
import sys


#TODO, eval is evil. Please use the AST or something similar.
class Module:
  def __init__(self, jsonPath):
    with open(jsonPath) as f:
      oldpath = os.getcwd()
      os.chdir(os.path.dirname(jsonPath))
      self.jsonObj = json.load(f)
      self.mapEntryPointsToBlocks()
      #Revert change to working directory.
      os.chdir(oldpath)

  def mapEntryPointsToBlocks(self):
    self.blocks = {}
    self.dllname = self.jsonObj["LibraryFilename"]
    #Determine the file extension based upon platform
    fileext = ""
    filepre = ""
    if sys.platform.lower() == "win32":
      fileext = ".dll"
    elif sys.platform.lower() == "linux2":
      fileext = ".so"
    elif sys.platform.lower() == "darwin":
      filepre = "lib"
      fileext = ".dylib"
    else:
      print "UNKNOWN PLATFORM!!!"
      quit(1)
    self.dllref = ctypes.cdll.LoadLibrary(filepre+self.dllname+fileext)
    for block in self.jsonObj["Blocks"]:
      self.blocks[block] = (self.Block(block,self.jsonObj["Blocks"][block],self.dllref))

  class Block:
    def __init__(self,blockName,blockJson,dllRef):
      func = eval("dllRef."+blockName)
      #Set input types
      func.inArgTypes = []
      func.inUnitTypes = []
      func.outArgNamess = []
      inputs = sorted(blockJson["Inputs"], key=lambda x: int(x["Ordinal"]))
      for inp in inputs:
        func.inArgTypes.append(eval(inp["PythonType"]))
        func.inUnitTypes.append(eval("Units."+inp["UnitType"]))

      #Outputs
      #Set our more interesting information
      func.outUnitTypes = []
      func.outArgTypes = []
      func.outArgName = []

      tmpOutUnitType = blockJson["Output"]["UnitType"]
      #Type unicode specifies a single output unit, not a struct
      if type(tmpOutUnitType) is unicode:
        func.restype = eval(blockJson["Output"]["PythonType"])
        func.outArgTypes.append(eval(blockJson["Output"]["PythonType"]))
        func.outUnitTypes.append(eval("Units."+tmpOutUnitType))
        func.outArgName.append(blockJson["Output"]["Name"])
      #Type list specifies a list of outputs, which _IS_ a struct
      elif type(tmpOutUnitType) is list:
        for outputUnit in tmpOutUnitType:
          func.outArgTypes.append(eval(outputUnit["PythonType"]))
          func.outUnitTypes.append(eval("Units."+outputUnit["UnitType"]))
          func.outArgName.append(str(outputUnit["Name"]))
        #With complete list of outputTypes and units construct a class
        #that inherits ctypes.Structure with the appropriate info
        className = str(blockJson["Output"]["Name"])
        func.restype = type(className, (ctypes.Structure,),{})
        func.restype._fields_ = zip(func.outArgName,func.outArgTypes)

      #Set ctypes expectations
      self.nativeFunc = func
      self.blockName = blockName
      self.dllRef = dllRef
      self.blockJson = blockJson

    def __call__(self,*args):
      if len(args) != len(self.nativeFunc.inArgTypes):
        sys.exit("Block called with incorrect number of arguments.") #More info?
      #Convert all types to destination types.
      nativeArgs = []
      #Test input argument validity
      for x in xrange(len(args)):
        var = self.nativeFunc.inUnitTypes[x](args[x])
      #Set native func arguments
      for x in xrange(len(args)):
        nativeArgs.append(self.nativeFunc.inArgTypes[x](args[x]))

      #Call the native function
      nativeRes = self.nativeFunc(*nativeArgs)

      #Result is a Struct
      if isinstance(nativeRes,ctypes.Structure):
        construct = {}
        for x in xrange(0,len(nativeRes._fields_)):
          fieldsPair = nativeRes._fields_[x]
          construct[fieldsPair[0]] = self.nativeFunc.outUnitTypes[x](getattr(nativeRes,fieldsPair[0]))
        return construct

      #Result is a single value
      else:
        construct = {}
        construct[self.nativeFunc.outArgName[0]] = self.nativeFunc.outUnitTypes[0](nativeRes)
        return construct

#Single file testing.
if __name__ == "__main__":
  import sys,os,json,ctypes
  testModule = Module('../Modules/BasicPropagation/opadesc.json')
  print "Test FSL: ",testModule.blocks["FreeSpaceLoss"](350000000,304.3)
  print "Test HaversineGeoInverse: ",testModule.blocks["HaversineGeoInverse"](0.0,1.0,0.0,1.0)
  print "Test EarthCurveHeightDecrease: ",testModule.blocks["EarthCurveHeightDecrease"](637108.8)
  print "Test HaversineGeo: ",testModule.blocks["HaversineGeo"](.123,.1230,.2130,10000)
  tworay = testModule.blocks["TwoRay"](50,50,1000,1,5.0,500000000,1)
  print "Test TwoRay: ",tworay
