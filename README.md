OPA: The Open Propagation Architechure
======================================
Purpose: To enable the quick prototyping an analysis of propagation models by quantizing them into easily replaceable modules.

Approach: Use a smart scripting front end with compiled modules to enable one to scaffold a model, and keep runtimes to a relative minimum.


## Tasks!
* Write c_types to opa module interface
* Write more complicated modules
* Support multiple modules
* Fix output units and automatically convert

## Required Software (PLEASE READ INSTALL.MD):
* A C compiler
* Python 2.x 64 bit
* > SCons 2.4.0 64 bit
* > Numpy 1.10.1 64 bit
